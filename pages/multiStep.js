import React, { useState } from 'react'
import Page1 from '../components/Page1';
import Page2 from '../components/Page2';

function multiStep() {
    const [page, setPage] = useState(1);

    function goNextPage() {
        setPage(page => page + 1);
    }
    return (
        <div className="">
           
            
           {page === 1 && <Page1/>}
           {page === 2 && <Page2/>}
           {page === 3 && <Page3/>}
           {page === 4 && <Page4/>}
           {page === 5 && <Page5/>}
           <div className="flex flex-col">

           <div className="">ProgressBar</div>

           <div className="flex justify-between ml-96">
           <button className="underline font-medium text-gray-800 hover:text-black">Retour</button>
           <button className="bg-gray-800 rounded-lg p-5 text-white font-medium content-end" onClick={goNextPage}>Suivant</button>
           
           </div>
          

           </div>
        </div>
    )
}





function Page3() {
    return (
        <div>I am page 3</div>
    )
}

export default multiStep
