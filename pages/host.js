import React from 'react'
import Form from '../components/Form'
import Header from '../components/Header'

function host() {
    return (
        <div>
            <Header />
            <Form/>
        </div>
    )
}

export default host
{/* <div className="grid grid-cols-2">
                <div>Image</div>
                <div className="bg-black">
                <div className=" text-center text-white px-24 h-screen space-y-7 mt-20 text-center">
                    <p className="text-center text-4xl">Devenez hôte en 10 étapes simples</p>
                <p className="text-center text-2xl">Rejoignez-nous. Nous allons vous aider pour chacune de ces étapes.</p>
                <div className="border text-gray-500 text-lg"/>
                <button className="bg-red-500 p-3 text-center">Démarrer</button>
                </div>
                </div>
            </div> */}