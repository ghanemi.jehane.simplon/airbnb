import React from 'react'

function Page2() {
    return (
        <div>
            <div className="grid grid-cols-2">
            <div className="bg-pink-600">
                <p className="text-white text-4xl text-center">Quel type de logement allez-vous proposer ?</p>
            </div>
            <div className="space-y-4">
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Appartement</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/eadbcbdb-d57d-44d9-9a76-665a7a4d1cd7.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Maison</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/d1af74db-58eb-46bf-b3f5-e42b6c9892db.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Annexe</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/32897901-1870-4895-8e23-f08dc0e61750.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Logement unique</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/7ad56bb1-ed9f-4dcb-a14c-2523da331b44.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Chambre d'hôtes</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/d52fb4e7-39a4-46df-9bf9-67e56d35eeca.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 space-x-56
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Boutique-hôtel</p>
                <div className="relative h-16 w-16 ">
                <Image src="https://a0.muscache.com/im/pictures/a2c9ad21-b159-4fd2-b417-d810fb23c6a9.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                
                
                
            </div>
            </div>
            
        </div>
    )
}

export default Page2
